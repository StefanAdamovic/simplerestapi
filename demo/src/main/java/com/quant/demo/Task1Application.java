package com.quant.demo;

import com.quant.demo.model.Person;
import com.quant.demo.repository.PersonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;

@SpringBootApplication
public class Task1Application{

    private static final Logger log = LoggerFactory.getLogger(Task1Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Task1Application.class, args);
        log.info("App starting.");

    }





}
