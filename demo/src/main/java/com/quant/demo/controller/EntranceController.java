package com.quant.demo.controller;


import com.quant.demo.model.AuthenticationRequest;
import com.quant.demo.model.AuthenticationResponse;
import com.quant.demo.service.implementation.JWTServiceImpl;
import com.quant.demo.service.implementation.UserDetailsServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class EntranceController {

    private static final Logger LOG = LoggerFactory.getLogger(EntranceController.class);

    private final AuthenticationManager authenticationManager;
    private UserDetailsServiceImpl userDetailsService;
    private JWTServiceImpl jwtService;

    @Autowired
    public EntranceController(
            UserDetailsServiceImpl userDetailsService,
            JWTServiceImpl jwtService, AuthenticationManager authenticationManager) {


        this.userDetailsService = userDetailsService;
        this.jwtService = jwtService;
        this.authenticationManager = authenticationManager;
    }

    @GetMapping("/welcome")
    public String getWelcomeMessage() {

        return "Welcome user";

    }

    @GetMapping("/user")
    public String getUserMessage() {
        return "Welcome User Role";
    }


    @GetMapping("/admin")
    public String getAdminMessage() {
        return "Welcome Admin Role";
    }

    @GetMapping("/member")
    public String getMemberMessage() {
        return "Welcome All members of app";
    }


    @GetMapping("/principal")
    public Principal retrievePrincipal(Principal principal) {
        return principal;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<AuthenticationResponse> createAuthenticationToken
            (@RequestBody AuthenticationRequest authenticationRequest) throws Exception {

        Authentication authentication =
                authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(authenticationRequest.getEmail(),
                                authenticationRequest.getPassword()));

        if (authentication.isAuthenticated()) {
            final UserDetails userDetails = userDetailsService
                    .loadUserByUsername(authenticationRequest.getEmail());

            final String jwt = jwtService.generateToken(userDetails);

            return ResponseEntity.ok(new AuthenticationResponse(jwt));
        } else return ResponseEntity.badRequest().build();
    }

}
