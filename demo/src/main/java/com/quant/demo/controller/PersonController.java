package com.quant.demo.controller;

import com.quant.demo.controller.handler.ResponseHandler;
import com.quant.demo.exception.EventNotMatchedException;
import com.quant.demo.model.Address;
import com.quant.demo.model.BankAccount;
import com.quant.demo.model.Event;
import com.quant.demo.model.Person;
import com.quant.demo.model.wrapper.PersonDto;
import com.quant.demo.service.AddressService;
import com.quant.demo.service.BankAccountService;
import com.quant.demo.service.EventService;
import com.quant.demo.service.PersonService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("persons")
public class PersonController {

    private static final Logger log = LoggerFactory.getLogger(PersonController.class);
    PersonService personService;
    BankAccountService bankAccountService;
    EventService eventService;
    AddressService addressService;

    @Autowired
    public PersonController(PersonService personService, BankAccountService bankAccountService, EventService eventService, AddressService addressService, BankAccountService bankAccountService1) {
        this.personService = personService;
        this.bankAccountService = bankAccountService;
        this.eventService = eventService;
        this.addressService = addressService;
        this.bankAccountService = bankAccountService1;
    }


    private static <T> ResponseEntity<Object> getResponseEntity(T item, String successMsg, String failMsg) {
        if (item instanceof Person && ((Person) item).getId() == 0
                || item instanceof PersonDto && ((PersonDto) item).getId() == 0
                || item instanceof BankAccount && ((BankAccount) item).getId() == 0) {
            return ResponseHandler.generateResponse(failMsg, HttpStatus.BAD_REQUEST);
        }
        return ResponseHandler.generateResponse(successMsg, HttpStatus.OK, item);
    }

    private static <T> ResponseEntity<Object> getResponseEntity(List<T> items, String succesMsg, String failMsg) {
        if (items.isEmpty())
            return ResponseHandler.generateResponse(failMsg, HttpStatus.BAD_REQUEST);
        return ResponseHandler.generateResponse(succesMsg, HttpStatus.OK, items);
    }

    @GetMapping
    public ResponseEntity<Object> getAllPerson() {
        log.info("Controller: Get mapping - get all");
        return getResponseEntity(personService.findAll(), "Success. Persons found!", "Fail. Persons not found!");
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getPersonById(@PathVariable int id) {

        return getResponseEntity(personService.findById(id), "Success. Person found!", "Fail. Person not found!");
    }

    @GetMapping("/{userId}/accounts")
    public ResponseEntity<Object> getPersonAccounts(@PathVariable long userId) {
        log.info("Controller: Get accounts for person by id");

        Person person = personService.findById(userId);

        return getResponseEntity(person.getBankAccounts(), "Success. Accounts found!", "Fail. Accounts not found!");
    }

    @GetMapping("/{userId}/accounts/{accountId}")
    public ResponseEntity<Object> getPersonAccountById(@PathVariable long userId, @PathVariable long accountId) {
        log.info("Controller: Get account for person by id");

        Person person = personService.findById(userId);

        BankAccount bankAccount = bankAccountService.findAccount(person, accountId);

        return getResponseEntity(bankAccount, "Success. Account found!", "Fail. Account not found!");
    }

    @GetMapping("/{userId}/addresses")
    public ResponseEntity<Object> getPersonAddress(@PathVariable long userId) {
        log.info("Controller: Get address for person by id");

        Person person = personService.findById(userId);

        return getResponseEntity(person.getAddress(), "Success. Address found!", "Fail. Address not found!");
    }

    @GetMapping("/{userId}/events")
    public ResponseEntity<Object> getPersonEvents(@PathVariable long userId) {
        log.info("Controller: Get events for person");

        Person person = personService.findById(userId);

        return getResponseEntity(person.getEvents(), "Success. Events found!", "Fail. Events not found!");
    }

    @GetMapping("/{userId}/events/{eventId}")
    public ResponseEntity<Object> getPersonEventById(@PathVariable long userId, @PathVariable long eventId) {
        log.info("Controller: Get account for person by id");

        Person person = personService.findById(userId);
        Event event = eventService.findAccount(person, eventId);

        return getResponseEntity(event, "Success. Event found!", "Fail. Event not found!");
    }

    @DeleteMapping("/{userId}/events/{eventId}")
    public ResponseEntity<Object> deletePersonEventById(@PathVariable long userId, @PathVariable long eventId) {
        log.info("Controller: Delete event for person by id");

        return getResponseEntity(eventService.deleteEventForPerson(eventId, userId), "Success. Event deleted!", "Fail. Event not deleted found!");

    }

    @PostMapping("/{userId}/events/{eventId}")
    public ResponseEntity<Object> addEventToPerson(@PathVariable long userId, @PathVariable long eventId) {
        log.info("Controller: Post add event for person");

        return getResponseEntity(eventService.addEventForPerson(eventId, userId), "Success. Event added!", "Fail. Event not added!");

    }

    @PostMapping("/{userId}/events")
    public ResponseEntity<Object> createEventForPerson(@PathVariable long userId, @RequestBody Event event) {
        return getResponseEntity(eventService.createEventForPerson(userId, event), "Success. Event Created!", "Fail. Event not created!");
    }


    @PostMapping
    public ResponseEntity<Object> addPerson(@RequestBody @Valid Person person) {
        log.info("Controller: Add new: " + person.toString());

        person.getBankAccounts().forEach((x) -> x.setPerson(person));

        return getResponseEntity(personService.save(person), "Success. Person added!", "Fail. Person not added!");
    }


    @PostMapping("/{userId}/accounts")
    public ResponseEntity<Object> createBankAccount(@PathVariable long userId, @RequestBody BankAccount bankAccount) {
        Person person = personService.findById(userId);
        bankAccount.setPerson(person);
        return getResponseEntity(bankAccountService.createBankAccount(bankAccount), "Success. Account added!", "Fail. Account not added!");
    }

    @PostMapping("/{userId}/addresses")
    public ResponseEntity<Object> addAddressToPerson(@PathVariable long userId, @RequestBody Address address) {
        Person person = personService.findById(userId);
        if (person.getAddress().getCity().equalsIgnoreCase("DefaultCity") || person.getAddress() == null) {

            address.getPersons().add(person);
            return ResponseHandler.generateResponse("Success. Address added!", HttpStatus.OK, addressService.createAddress(address));
        }
        return ResponseHandler.generateResponse("Person already have address.", HttpStatus.BAD_REQUEST, person.getAddress());
    }

    @PostMapping("/saveAll")
    public ResponseEntity<Object> addPersons(@RequestBody @Valid List<Person> persons) {

        List<Person> notSavedPersons = personService.saveAll(persons);
        if (notSavedPersons.isEmpty())
            return ResponseHandler.generateResponse("Success. All Persons saved!", HttpStatus.CREATED, persons);
        return ResponseHandler.generateResponse("Persons not saved.", HttpStatus.BAD_REQUEST, notSavedPersons);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updatePerson(@PathVariable int id, @RequestBody @Valid Person person) {
        log.info("Controller: Update person: " + person.toString());
        person.setId(id);

        return getResponseEntity(personService.updatePerson(person), "Success. Person updated!", "Fail! Person not updated!");
    }

    @PutMapping("/{userId}/accounts/{accountId}")
    public ResponseEntity<Object> updatePersonAccount(@PathVariable long userId, @PathVariable long accountId, @RequestBody BankAccount bankAccount) {
        bankAccount.setId(accountId);
        Person person = personService.findById(userId);
        bankAccount.setPerson(person);
        return getResponseEntity(bankAccountService.updateBankAccount(bankAccount), "Success. Account updated!", "Fail. Account not updated!");
    }

    @PutMapping("/{userId}/addresses/{addressId}")
    public ResponseEntity<Object> updatePersonAddress(@PathVariable long userId, @PathVariable long addressId, @RequestBody Address address) {

        Person person = personService.findById(userId);
        if (person.getAddress().getCity().equalsIgnoreCase("DefaultCity") || person.getAddress() == null) {
            return ResponseHandler.generateResponse("Person doesnt have address, forward to creation", HttpStatus.OK, addAddressToPerson(userId, address));
        }
        address.setId(addressId);
        address.getPersons().add(person);
        return ResponseHandler.generateResponse("Success. Address updated!", HttpStatus.BAD_REQUEST, addressService.updateAddress(address));

    }


    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deletePerson(@PathVariable int id) {
        log.info("Controller: Delete user with id: " + id);
        PersonDto personDto = new PersonDto(personService.delete(id));
        return getResponseEntity(personDto, "Success. Person with id: " + id + " deleted!", "Fail. Person with id: " + id + " doesnt exist!");
    }

    @DeleteMapping("/{userId}/accounts/{accountId}")
    public ResponseEntity<Object> deletePersonAccount(@PathVariable long userId, @PathVariable long accountId) {

        return getResponseEntity(bankAccountService.delete(accountId), "Success. Account deleted!", "Fail. Account not deleted!");
    }

    @DeleteMapping("/{userId}/addresses")
    public ResponseEntity<Object> deletePersonAddress(@PathVariable long userId) {

        long addressId = personService.findById(userId).getAddress().getId();
        return getResponseEntity(addressService.delete(addressId), "Success. Address deleted!", "Fail. Address not deleted!");

    }
}
