package com.quant.demo.exception;

import java.io.Serial;

public class EventNotMatchedException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 2L;

    public EventNotMatchedException(String msg) {
        super(msg);
    }
}
