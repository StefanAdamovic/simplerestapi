package com.quant.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private long id;
    private String city;
    private String streetName;
    private int streetNumber;
    @OneToMany(mappedBy = "address", cascade = {CascadeType.PERSIST
            , CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH},
            fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Person> persons;

    public Address() {
        id = 0;
        city = "DefaultCity";
        streetName = "DefaultStreet";
        streetNumber = 0;
        persons = new ArrayList<>();

    }

    public Address(long id, String city, String streetName, int streetNumber, List<Person> persons) {
        this.id = id;
        this.city = city;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.persons = persons;
    }

    public Address(String city, String streetName, int streetNumber, List<Person> persons) {
        this.city = city;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.persons = persons;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }


    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", city='" + city + '\'' +
                ", streetName='" + streetName + '\'' +
                ", streetNumber=" + streetNumber +
                ", persons=" + persons +
                '}';
    }
}
