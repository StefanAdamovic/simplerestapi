package com.quant.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

@Entity
public class BankAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JsonIgnore
    private Person person;

    private long balance;


    public BankAccount(long id, String name, Person person, long balance) {
        this.id = id;
        this.name = name;
        this.person = person;
        this.balance = balance;
    }

    public BankAccount(String name, Person person, long balance) {
        this.name = name;
        this.person = person;
        this.balance = balance;
    }

    public BankAccount() {
        this.id = 0;
        this.name = "DefaultBankAccount";
        //this.person = new Person();
        this.balance = 0;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", person=" + person +
                ", balance=" + balance +
                '}';
    }
}
