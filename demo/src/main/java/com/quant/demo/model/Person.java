package com.quant.demo.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static jakarta.persistence.EnumType.STRING;

@Entity
public class Person implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    @NotBlank(message = "Email is null or blank")
    private String email;

    @Column
    @NotBlank(message = "Password is null or blank")
    private String password;

    @Column
    @NotBlank(message = "First name is null or blank")
    private String firstName;


    @Column
    @NotBlank(message = "Last name is null or blank")
    private String lastName;

    @Column
    @NotNull(message = "Date is null")
    private LocalDate dateOfBirth;

    @Transient
    private int age;

    @Column(unique = true)
    private int birthRegistrationId;

    @ManyToOne
    @JoinColumn(name = "address_id", nullable = true)
    @JsonIgnore
    private Address address;

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<BankAccount> bankAccounts;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.DETACH})
    @JoinTable
            (name = "person_event", joinColumns = @JoinColumn(name = "person_id"),
                    inverseJoinColumns = @JoinColumn(name = "event_id")
            )
    @JsonIgnore
    private List<Event> events;

    @Enumerated(STRING)
    private UserRole role;

    public Person() {
        id = 0;
        firstName = "DefaultFirstName";
        lastName = "DefaultLastName";
        birthRegistrationId = 0;
        dateOfBirth = LocalDate.now();
        address = new Address();
        bankAccounts = new ArrayList<>();
        events = new ArrayList<>();
        email = "DefaultEmail";
        password = "DefaultPassword";
    }

    public Person(
            long id, String firstName, String lastName, LocalDate dateOfBirth,
            int age, int birthRegistrationId, Address address, List<BankAccount> bankAccounts,
            List<Event> events, UserRole role, String password, String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.age = age;
        this.birthRegistrationId = birthRegistrationId;
        this.address = address;
        this.bankAccounts = bankAccounts;
        this.events = events;
        this.role = role;
        this.email = email;
        this.password = password;
    }

    public Person(String firstName, String lastName, LocalDate dateOfBirth,
                  int age, int birthRegistrationId, Address address, List<BankAccount> bankAccounts,
                  List<Event> events, UserRole role, String password, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.age = age;
        this.birthRegistrationId = birthRegistrationId;
        this.address = address;
        this.bankAccounts = bankAccounts;
        this.events = events;
        this.role = role;
        this.email = email;
        this.password = password;
    }

    public Person(String firstName, String lastName, LocalDate dateOfBirth, int birthRegistrationId, UserRole role, String password, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.birthRegistrationId = birthRegistrationId;
        this.role = role;
        this.email = email;
        this.password = password;
    }


    public Person(Person person) {

        this.id = person.getId();
        this.firstName = person.getFirstName();
        this.lastName = person.getLastName();
        this.dateOfBirth = person.getDateOfBirth();
        this.age = person.getAge();
        this.birthRegistrationId = person.getBirthRegistrationId();
        this.address = person.getAddress();
        this.bankAccounts = person.getBankAccounts();
        this.events = person.getEvents();
        this.role = person.getRole();
        this.email = person.getEmail();
        this.password = person.getPassword();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", age=" + age +
                ", birthRegistrationId=" + birthRegistrationId +
                ", role=" + role +
                '}';
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public int getBirthRegistrationId() {
        return birthRegistrationId;
    }

    public void setBirthRegistrationId(int birthRegistrationId) {
        this.birthRegistrationId = birthRegistrationId;
    }

    public int getAge() {
        return Period.between(this.dateOfBirth, LocalDate.now()).getYears();
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate date) {
        this.dateOfBirth = date;
    }

    public List<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    public void setBankAccounts(List<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(role.name()));
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getUsername() {
        return this.email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
