package com.quant.demo.model;

public enum UserRole {

    ROLE_USER, ROLE_ADMIN
}
