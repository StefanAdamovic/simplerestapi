package com.quant.demo.model.wrapper;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.quant.demo.model.Address;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AddressDto {

    private long id;
    private String city;
    private String streetName;
    private int streetNumber;


    public AddressDto() {
        id = 0;
        city = "DefaultCity";
        streetName = "DefaultStreet";
        streetNumber = 0;

    }

    public AddressDto
            (long id, String city, String streetName, int streetNumber, List<PersonDto> persons,
             List<EventDto> events) {
        this.id = id;
        this.city = city;
        this.streetName = streetName;
        this.streetNumber = streetNumber;

    }

    public AddressDto
            (String city, String streetName, int streetNumber, List<PersonDto> persons,
             List<EventDto> events) {
        this.city = city;
        this.streetName = streetName;
        this.streetNumber = streetNumber;

    }


    public AddressDto(Address address) {
        this.id = address.getId();
        this.city = address.getCity();
        this.streetName = address.getStreetName();
        this.streetNumber = address.getStreetNumber();

    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }

    @Override
    public String toString() {
        return "AddressDto{" +
                "id=" + id +
                ", city='" + city + '\'' +
                ", streetName='" + streetName + '\'' +
                ", streetNumber=" + streetNumber +
                '}';
    }
}
