package com.quant.demo.model.wrapper;

import com.quant.demo.model.BankAccount;
import org.springframework.stereotype.Component;

@Component
public class BankAccountDto {

    private long id;

    private String name;

    //private PersonDto person;

    private long balance;


    public BankAccountDto() {
        this.id = 0;
        this.name = "DefaultBankAccount";
        //this.person = new PersonDto();
        this.balance = 0;
    }

    public BankAccountDto(long id, String name, long balance) {
        this.id = id;
        this.name = name;
        //this.person = person;
        this.balance = balance;
    }

    public BankAccountDto(String name, long balance) {
        this.name = name;
        //this.person = person;
        this.balance = balance;
    }

    public BankAccountDto(BankAccount bankAccount) {
        this.name = bankAccount.getName();
        //this.person = new PersonDto(bankAccount.getPerson());
        this.balance = bankAccount.getBalance();
        this.id = bankAccount.getId();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public PersonDto getPerson() {
//        return person;
//    }
//
//    public void setPerson(PersonDto person) {
//        this.person = person;
//    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "BankAccountDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                '}';
    }
}
