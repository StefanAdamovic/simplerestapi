package com.quant.demo.model.wrapper;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.quant.demo.model.Event;
import com.quant.demo.model.Person;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EventDto {

    private long id;

    private String name;

    @JsonIgnore
    private List<Person> people;


    public EventDto() {
        this.id = 0;
        this.name = "DefaultEvent";
        this.people = new ArrayList<>();
    }

    public EventDto(long id, String name, List<Person> people) {
        this.id = id;
        this.name = name;

        this.people = people;
    }

    public EventDto(String name, List<Person> people) {
        this.name = name;
        this.people = people;
    }

    public EventDto(Event event) {
        this.name = event.getName();
        this.id = event.getId();
        this.people = event.getPeople();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Person> getPeople() {
        return people;
    }

    public void setPeople(List<Person> people) {
        this.people = people;
    }

    @Override
    public String toString() {
        return "EventDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", people=" + people +
                '}';
    }
}
