package com.quant.demo.model.wrapper;

import com.quant.demo.model.Person;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class PersonDto {

    private long id;

    private String firstName;

    private String lastName;

    private LocalDate dateOfBirth;

    private int age;

    private int birthRegistrationId;

    private AddressDto address;

    private List<BankAccountDto> bankAccounts;

    private List<EventDto> events;


    public PersonDto() {
        id = 0;
        firstName = "DefaultFirstName";
        lastName = "DefaultLastName";
        birthRegistrationId = 0;
        dateOfBirth = LocalDate.now();
        address = new AddressDto();
        bankAccounts = new ArrayList<>();
        events = new ArrayList<>();
    }

    public PersonDto(Person person) {
        id = person.getId();
        firstName = person.getFirstName();
        lastName = person.getLastName();
        birthRegistrationId = person.getBirthRegistrationId();
        dateOfBirth = person.getDateOfBirth();
        address = new AddressDto(person.getAddress());
        bankAccounts = new ArrayList<>();
        age = person.getAge();

        person.getBankAccounts().forEach((accountObj) -> {
            bankAccounts.add(new BankAccountDto(accountObj));
        });

        events = new ArrayList<>();

        person.getEvents().forEach((eventObj) -> {
            events.add(new EventDto(eventObj));
        });
    }

    public PersonDto
            (long id, String firstName, String lastName, LocalDate dateOfBirth, int age,
             int birthRegistrationId, AddressDto address, List<BankAccountDto> bankAccounts, List<EventDto> events) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.age = age;
        this.birthRegistrationId = birthRegistrationId;
        this.address = address;
        this.bankAccounts = bankAccounts;
        this.events = events;
    }

    public PersonDto
            (String firstName, String lastName, LocalDate dateOfBirth, int age,
             int birthRegistrationId, AddressDto address, List<BankAccountDto> bankAccounts, List<EventDto> events) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.age = age;
        this.birthRegistrationId = birthRegistrationId;
        this.address = address;
        this.bankAccounts = bankAccounts;
        this.events = events;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getBirthRegistrationId() {
        return birthRegistrationId;
    }

    public void setBirthRegistrationId(int birthRegistrationId) {
        this.birthRegistrationId = birthRegistrationId;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public List<BankAccountDto> getBankAccounts() {
        return bankAccounts;
    }

    public void setBankAccounts(List<BankAccountDto> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    public List<EventDto> getEvents() {
        return events;
    }

    public void setEvents(List<EventDto> events) {
        this.events = events;
    }

    @Override
    public String toString() {
        return "PersonDto{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", age=" + age +
                ", birthRegistrationId=" + birthRegistrationId +
                ", address=" + address +
                ", bankAccounts=" + bankAccounts +
                ", events=" + events +
                '}';
    }
}
