package com.quant.demo.repository;

import com.quant.demo.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {


    //@Query("select p from Person p where p.birthRegistrationId = ?1" )
    Optional<Person> findByBirthRegistrationId(int birthId);


    @Query("SELECT p FROM Person p WHERE p.email = :email")
    Optional<Person> findByEmail(String email);

    @Query("SELECT birthRegistrationId FROM Person")
    List<Integer> getAllBirthIds();


    List<Person> findByDateOfBirthBetweenOrderByDateOfBirthDesc(LocalDate starDate, LocalDate endDate);
}
