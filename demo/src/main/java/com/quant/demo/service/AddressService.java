package com.quant.demo.service;

import com.quant.demo.model.Address;

import java.util.List;

public interface AddressService {

    List<Address> getAllAddresses();

    Address createAddress(Address address);

    Address delete(long id);

    Address updateAddress(Address address);

    Address findById(long id);
}
