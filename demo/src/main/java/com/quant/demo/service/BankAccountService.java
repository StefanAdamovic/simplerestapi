package com.quant.demo.service;

import com.quant.demo.model.BankAccount;
import com.quant.demo.model.Person;

import java.util.List;

public interface BankAccountService {

    List<BankAccount> getAllAccounts();

    BankAccount createBankAccount(BankAccount bankAccount);

    BankAccount delete(long id);

    BankAccount updateBankAccount(BankAccount bankAccount);

    BankAccount findById(long id);

    BankAccount findAccount(Person person, long id);

}
