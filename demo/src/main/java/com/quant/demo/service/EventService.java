package com.quant.demo.service;

import com.quant.demo.model.BankAccount;
import com.quant.demo.model.Event;
import com.quant.demo.model.Person;

import java.util.List;

public interface EventService {

    List<Event> getAllEvents();

    Event createEvent(Event event);

    Event delete(long id);

    Event updateEvent(Event event);

    Event findById(long id);

    Event findAccount(Person person, long id);

    Event deleteEventForPerson(long id, long userId);

    Event addEventForPerson(long id, long userId);

    Event createEventForPerson(long userId, Event event);
}
