package com.quant.demo.service;

import com.quant.demo.model.Person;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface PersonService {

    List<Person> findAll();

    Person save(Person person);

    List<Person> saveAll(List<Person> persons);

    Person delete(Person person);

    Person delete(long id);

    Person updatePerson(Person person);

    Person findById(long id);

    public UserDetailsService userDetailsService();

}
