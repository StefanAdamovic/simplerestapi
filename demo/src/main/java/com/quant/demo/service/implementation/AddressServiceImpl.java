package com.quant.demo.service.implementation;

import com.quant.demo.model.Address;
import com.quant.demo.repository.AddressRepository;
import com.quant.demo.repository.PersonRepository;
import com.quant.demo.service.AddressService;
import com.quant.demo.service.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AddressServiceImpl implements AddressService {

    private static final Logger log = LoggerFactory.getLogger(AddressServiceImpl.class);
    AddressRepository repository;
    PersonRepository personService;

    @Autowired
    public AddressServiceImpl(AddressRepository repository, PersonRepository personRepository) {
        this.repository = repository;
        this.personService = personRepository;
    }


    @Override
    public List<Address> getAllAddresses() {
        return repository.findAll();
    }

    @Override
    public Address createAddress(Address address) {

        if (repository.findAll().stream().anyMatch(a -> a.equals(address))) {
            log.error("Cant create Address, already exists");
            return new Address();
        }

        return repository.save(address);
    }

    @Override
    public Address delete(long id) {
        Optional<Address> addressOptional = repository.findById(id);
        if (addressOptional.isEmpty()) {
            log.error("Not able to delete.Address with id:" + id);
            return new Address();
        }

        Address address = addressOptional.get();

        address.getPersons().forEach((p) -> {
            p.setAddress(null);
            personService.save(p);

        });

        repository.delete(address);

        return addressOptional.get();
    }

    @Override
    public Address updateAddress(Address address) {

        Optional<Address> addressOptional = repository.findById(address.getId());

        if (addressOptional.isEmpty()) {
            log.error("Not able to update.Person with id:" + address.getId());
            return new Address();
        }


        if (repository.findAll().stream().anyMatch(a -> a.equals(address))) {
            log.error("Cant update Address, already exists");
            return new Address();
        }

        Address updatedAddress = addressOptional.get();
        updatedAddress.setCity(address.getCity());
        updatedAddress.setStreetName(address.getStreetName());
        updatedAddress.setStreetNumber(address.getStreetNumber());
        updatedAddress.getPersons().addAll(address.getPersons());

        repository.save(updatedAddress);
        return updatedAddress;
    }

    @Override
    public Address findById(long id) {
        Optional<Address> addressOptional = repository.findById(id);
        if (addressOptional.isEmpty()) {
            log.error("Not able to find Address with id:" + id);
            return new Address();
        }
        return addressOptional.get();
    }
}
