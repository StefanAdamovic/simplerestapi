package com.quant.demo.service.implementation;

import com.quant.demo.model.BankAccount;
import com.quant.demo.model.Person;
import com.quant.demo.repository.BankAccountRepository;
import com.quant.demo.service.BankAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BankAccountServiceImpl implements BankAccountService {

    private static final Logger log = LoggerFactory.getLogger(BankAccountServiceImpl.class);
    BankAccountRepository repository;

    @Autowired
    public BankAccountServiceImpl(BankAccountRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<BankAccount> getAllAccounts() {
        return repository.findAll();
    }

    @Override
    public BankAccount createBankAccount(BankAccount bankAccount) {
        repository.save(bankAccount);
        return bankAccount;
    }

    @Override
    public BankAccount delete(long id) {
        Optional<BankAccount> bankAccountOptional = repository.findById(id);
        if (bankAccountOptional.isEmpty()) {
            log.error("Not able to delete. Account with id:" + id);
            return new BankAccount();
        }
        repository.deleteById(id);
        return bankAccountOptional.get();
    }

    @Override
    public BankAccount updateBankAccount(BankAccount bankAccount) {

        Optional<BankAccount> bankAccountOptional = repository.findById(bankAccount.getId());

        if (bankAccountOptional.isEmpty()) {
            log.error("Not able to update. Account with id:" + bankAccount.getId());
            return new BankAccount();
        }


        BankAccount updatedAccount = bankAccountOptional.get();
        updatedAccount.setName(bankAccount.getName());
        updatedAccount.setBalance(bankAccount.getBalance());
        //updatedAccount.setPerson(bankAccount.getPerson());
        return repository.save(updatedAccount);
    }

    @Override
    public BankAccount findById(long id) {
        Optional<BankAccount> bankAccountOptional = repository.findById(id);
        if (bankAccountOptional.isEmpty()) {
            log.error("Not able to update. Account with id:" + id);
            return new BankAccount();
        }
        return bankAccountOptional.get();
    }

    @Override
    public BankAccount findAccount(Person person, long id) {
        List<BankAccount> personAccounts = person.getBankAccounts();

        if (!personAccounts.stream().anyMatch((perAccount) -> perAccount.getId() == id)) {
            log.error("Account doesnt Match.No Account with id:" + id);
            return new BankAccount();
        }

        Optional<BankAccount> bankAccountOptional = repository.findById(id);

        return bankAccountOptional.orElseGet(BankAccount::new);

    }

}
