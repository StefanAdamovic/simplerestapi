package com.quant.demo.service.implementation;

import com.quant.demo.exception.EventNotMatchedException;
import com.quant.demo.exception.ResourceNotFoundException;
import com.quant.demo.model.Address;
import com.quant.demo.model.BankAccount;
import com.quant.demo.model.Event;
import com.quant.demo.model.Person;
import com.quant.demo.repository.EventRepository;
import com.quant.demo.repository.PersonRepository;
import com.quant.demo.service.EventService;
import com.quant.demo.service.PersonService;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

@Service
public class EventServiceImpl implements EventService {

    private static final Logger log = LoggerFactory.getLogger(EventServiceImpl.class);
    EventRepository repository;
    PersonRepository personRepository;

    @Autowired
    public EventServiceImpl(EventRepository repository, PersonRepository personRepository) {
        this.repository = repository;
        this.personRepository = personRepository;
    }

    @Override
    public List<Event> getAllEvents() {
        return repository.findAll();
    }

    @Override
    public Event createEvent(Event event) {
        return repository.save(event);
    }

    @Override
    public Event delete(long id) {
        Optional<Event> eventOptional = repository.findById(id);
        if (eventOptional.isEmpty()) {
            log.error("Not able to delete. Event with id:" + id);
            return new Event();
        }
        Event event = eventOptional.get();

        event.getPeople().forEach((p) -> {
            p.getEvents().removeIf((pEvent) -> pEvent.getId() == id);
            personRepository.save(p);

        });

        repository.delete(event);

        return eventOptional.get();
    }

    @Override
    public Event deleteEventForPerson(long id, long userId) {
        Optional<Event> eventOptional = repository.findById(id);
        Optional<Person> personOptional = personRepository.findById(userId);

        if (personOptional.isPresent() && eventOptional.isPresent()) {
            Person person = personOptional.get();

            if (person.getEvents().stream().anyMatch((pEvent) -> pEvent.getId() == id)) {
                Event event = eventOptional.get();

                person.getEvents().remove(event);
                personRepository.save(person);
                return event;
            }
            throw new EventNotMatchedException("Person doesnt have event you want to delete from him");

        }
        throw new EventNotMatchedException("There is no person or event you want to delete");
    }

    @Override
    public Event addEventForPerson(long id, long userId) {
        Optional<Event> eventOptional = repository.findById(id);
        Optional<Person> personOptional = personRepository.findById(userId);

        if (personOptional.isPresent() && eventOptional.isPresent()) {
            Person person = personOptional.get();

            if (person.getEvents().stream().noneMatch((pEvent) -> pEvent.getId() == id)) {
                Event event = eventOptional.get();

                person.getEvents().add(event);
                personRepository.save(person);
                return event;
            }
            throw new EventNotMatchedException("Person already have that event");

        }
        throw new ResourceNotFoundException("There is no person or event you want to add.");
    }

    @Override
    public Event createEventForPerson(long userId, Event event) {
        Optional<Person> personOptional = personRepository.findById(userId);

        if (personOptional.isPresent()) {
            Person person = personOptional.get();

            if (person.getEvents().stream().noneMatch((pEvent) -> pEvent.getName().equalsIgnoreCase(event.getName()))) {

                person.getEvents().add(repository.save(event));
                personRepository.save(person);
                return event;
            }
            throw new EventNotMatchedException("Person already have that event");

        }
        throw new ResourceNotFoundException("There is no person you want to create event.");
    }

    @Override
    public Event updateEvent(Event event) {

        Optional<Event> eventOptional = repository.findById(event.getId());

        if (eventOptional.isEmpty()) {
            log.error("Not able to update. Event with id:" + event.getId());
            return new Event();
        }


        Event updatedEvent = eventOptional.get();
        updatedEvent.setName(event.getName());
        //updatedEvent.setAddress(event.getAddress());
        updatedEvent.setPeople(event.getPeople());
        return repository.save(updatedEvent);
    }

    @Override
    public Event findById(long id) {
        Optional<Event> eventOptional = repository.findById(id);
//        if (eventOptional.isEmpty()) {
//            log.error("Not able to update. Event with id:" + id);
//            throw new ResourceNotFoundException("Not found Event with id = " + id);
//        }
        return eventOptional.orElseThrow(() -> new ResourceNotFoundException("Not found Event with id = " + id));
    }

    @Override
    public Event findAccount(Person person, long id) {
        List<Event> personEvents = person.getEvents();

        if (personEvents.stream().noneMatch((perAccount) -> perAccount.getId() == id)) {
            log.error("Account doesnt Match.No Event with id:" + id);
            throw new ResourceNotFoundException("Not found Event with id = " + id);
        }

        Optional<Event> eventOptional = repository.findById(id);

        return eventOptional.orElseGet(Event::new);

    }
}
