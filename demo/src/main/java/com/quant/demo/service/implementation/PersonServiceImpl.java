package com.quant.demo.service.implementation;

import com.quant.demo.exception.ResourceNotFoundException;
import com.quant.demo.model.Person;
import com.quant.demo.model.wrapper.PersonDto;
import com.quant.demo.repository.PersonRepository;
import com.quant.demo.service.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PersonServiceImpl implements PersonService {
    private static final Logger log = LoggerFactory.getLogger(PersonServiceImpl.class);
    PersonRepository repository;
    PersonDto personDto;

    @Autowired
    public PersonServiceImpl(PersonRepository repository, PersonDto personDto) {
        this.repository = repository;
    }

    public UserDetailsService userDetailsService() {
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                return repository.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("User not found"));
            }
        };
    }

    @Override
    public List<Person> findAll() {
        return repository.findAll();
    }

    @Override
    public Person save(Person person) {

        Optional<Person> personOptional = repository.findByBirthRegistrationId
                (person.getBirthRegistrationId());

        if (personOptional.isPresent()) {
            log.error("Person with that birth id already exists");
            return new Person();
        }
        repository.save(person);
        return person;
    }

    @Override
    public List<Person> saveAll(List<Person> persons) {

        List<Integer> birthIds = repository.getAllBirthIds();
        List<Person> personsToReturn = new ArrayList<>();
        List<Person> personsToSave = new ArrayList<>();

        persons.forEach(person -> {
                    if (birthIds.stream().noneMatch(id -> id == person.getBirthRegistrationId())) {
                        personsToSave.add(person);
                    } else {
                        personsToReturn.add(person);
                    }
                }
        );
        log.info("Persons for save before save: " + persons);

        if (personsToSave.isEmpty()) {
            return personsToReturn;
        }
        repository.saveAll(personsToSave);
        log.info("Persons not saved: " + personsToReturn);
        return personsToReturn;
    }

    @Override
    public Person delete(Person person) {

        Optional<Person> personOptional = repository.findById((person.getId()));
        if (personOptional.isEmpty()) {
            log.error("Not able to delete.Person with id:" + person.getId() + ", doesnt exists in applicaiton.");
            return new Person();
        }
        Person rePerson = new Person(personOptional.get());
        person.getEvents();
        person.getAddress();
        person.getBankAccounts();
        repository.delete(person);
        return person;
    }

    public Person delete(long id) {

        Optional<Person> personOptional = repository.findById(id);
        if (personOptional.isEmpty()) {
            log.error("Not able to delete.Person with id:" + id + ", doesnt exists in applicaiton.");
            return new Person();
        }
        Person person = personOptional.get();
        repository.deleteById(id);
        return person;
    }

    @Override
    public Person findById(long id) {
        Optional<Person> personOptional = Optional.ofNullable(
                repository.findById(id).orElseThrow(
                        () -> new ResourceNotFoundException("Not found Person with id = " + id)));
        ;
        return personOptional.orElse(null);
    }

    @Override
    public Person updatePerson(Person person) {

        Optional<Person> optionalPerson = repository.findById(person.getId());

        if (optionalPerson.isEmpty()) {
            log.error("Not able to update.Person with id:" + person.getId() + ", doesnt exists in applicaiton.");
            return new Person();
        }


        if (repository.getAllBirthIds().stream().anyMatch((id) -> id.equals(person.getBirthRegistrationId()))) {
            log.error("Person with that birth id already exists");
            return new Person();
        }

        Person updatedPerson = optionalPerson.get();
        updatedPerson.setFirstName(person.getFirstName());
        updatedPerson.setLastName(person.getLastName());
        updatedPerson.setDateOfBirth(person.getDateOfBirth());
        updatedPerson.setBirthRegistrationId(person.getBirthRegistrationId());

        repository.save(updatedPerson);
        return updatedPerson;
    }


}
