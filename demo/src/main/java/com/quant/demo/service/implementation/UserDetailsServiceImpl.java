package com.quant.demo.service.implementation;

import com.quant.demo.model.Person;
import com.quant.demo.repository.PersonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private static final Logger LOG = LoggerFactory.getLogger(UserDetailsServiceImpl.class);
    private final PersonRepository personRepository;

    @Autowired
    public UserDetailsServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LOG.info(username);


        Person person = personRepository.findByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username not found"));


        person.setEvents(null);
        person.setBankAccounts(null);
        person.setAddress(null);

        LOG.info(person.toString());
        return person;
    }
}
