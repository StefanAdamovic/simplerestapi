-- Insert sample Address data
INSERT INTO address (city, street_name, street_number) VALUES ('City1', 'StreetName1', 123);
INSERT INTO address (city, street_name, street_number) VALUES ('City2', 'StreetName2', 456);

-- Insert sample Person data
INSERT INTO person (first_name, last_name, date_of_birth, birth_registration_id, address_id, role, email, password) VALUES ('John', 'Doe', '1990-01-15', 12345, 1,'ROLE_USER', 'user@user.com', 12);
INSERT INTO person (first_name, last_name, date_of_birth, birth_registration_id, address_id, role, email, password) VALUES ('Jane', 'Smith', '1985-05-20', 67890, 2,'ROLE_ADMIN','admin@admin.com', 1234);

-- Insert sample BankAccount data
INSERT INTO bank_account (name, person_id, balance) VALUES ('Account1', 1, 1000);
INSERT INTO bank_account (name, person_id, balance) VALUES ('Account2', 2, 2000);

-- Insert sample Event data
INSERT INTO event (name) VALUES ('Event1');
INSERT INTO event (name) VALUES ('Event2');

-- Add relationships for Person-Event and Event-Person (if you have many-to-many relationships)
INSERT INTO person_event (person_id, event_id) VALUES (1, 1);
INSERT INTO person_event (person_id, event_id) VALUES (1, 2);
INSERT INTO person_event (person_id, event_id) VALUES (2, 1);
